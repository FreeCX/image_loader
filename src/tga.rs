use std::io::{Read, Write};
use std::path::Path;
use std::fs::File;
use std::u16;
use super::extra::*;

#[derive(Debug)]
struct TGAHeader {
    id_len: u8,
    color_map_type: u8,
    data_type_code: u8,
    color_map_origin: u16,
    color_map_length: u16,
    color_map_depth: u8,
    x_origin: u16,
    y_origin: u16,
    width: u16,
    height: u16,
    bits_per_pixel: u8,
    image_descriptor: u8,
}

#[derive(Debug)]
pub struct TGAImage {
    header: TGAHeader,
    image: Vec<Color>,
}

impl TGAImage {
    pub fn new(width: u16, height: u16) -> Option<TGAImage> {
        let header = TGAHeader {
            id_len: 0,
            color_map_type: 0,
            data_type_code: 2,
            color_map_origin: 0,
            color_map_length: 0,
            color_map_depth: 0,
            x_origin: 0,
            y_origin: 0,
            width: width,
            height: height,
            bits_per_pixel: 24,
            image_descriptor: 0,
        };
        let image = vec![Color::black(); width as usize * height as usize];
        Some(TGAImage {
            header: header,
            image: image,
        })
    }

    pub fn set(&mut self, x: u16, y: u16, color: Color) {
        let position = y as usize * self.header.width as usize + x as usize;
        *(self.image.get_mut(position).unwrap()) = color;
    }

    pub fn get(&self, x: u16, y: u16) -> Color {
        let position = y as usize * self.header.width as usize + x as usize;
        *(self.image.get(position).unwrap())
    }

    // TODO: optimize this
    pub fn flip_horizontally(&mut self) {
        if self.image.len() > 0 {
            self.flip_vertically();
            self.image.reverse();
        }
    }

    pub fn flip_vertically(&mut self) {
        if self.image.len() > 0 {
            let width = self.header.width as usize;
            let height = self.header.height as usize;
            let half = width >> 1;
            let max_x = width - 1;
            for y in 0..height {
                let max_y = width * y;
                for x in 0..half {
                    self.image.swap(max_y + x, max_y + max_x - x);
                }
            }
        }
    }

    // TODO: rewrite for working with RGBA & RLE
    pub fn read_from_buffer(buf: &[u8]) -> Option<TGAImage> {
        let color_map_origin = get_u16(&buf[3..], Order::LittleEndian);
        let color_map_length = get_u16(&buf[5..], Order::LittleEndian);
        let x_origin = get_u16(&buf[8..], Order::LittleEndian);
        let y_origin = get_u16(&buf[10..], Order::LittleEndian);
        let width = get_u16(&buf[12..], Order::LittleEndian);
        let height = get_u16(&buf[14..], Order::LittleEndian);
        // read TGA header
        let header = TGAHeader {
            id_len: buf[0],
            color_map_type: buf[1],
            data_type_code: buf[2],
            color_map_origin: color_map_origin,
            color_map_length: color_map_length,
            color_map_depth: buf[7],
            x_origin: x_origin,
            y_origin: y_origin,
            width: width,
            height: height,
            bits_per_pixel: buf[16],
            image_descriptor: buf[17],
        };
        let mut img: Vec<Color> = Vec::new();
        let image_size: usize = 3 * (width as usize) * (height as usize);
        let iterator: Vec<u8> = buf[18..].iter().take(image_size).map(|&x| x).collect();
        for data in iterator.as_slice().chunks(3) {
            // really?
            let color = Color::bgr_v(data);
            img.push(color);
        }
        Some(TGAImage {
            header: header,
            image: img,
        })
    }

    pub fn read_from_file<P: AsRef<Path>>(file: P) -> Option<TGAImage> {
        let mut file = try_it!(File::open(file));
        let mut data = Vec::new();
        try_it!(file.read_to_end(&mut data));
        TGAImage::read_from_buffer(&mut data)
    }

    pub fn write_to_file<P: AsRef<Path>>(&self, file: P) {
        let mut file = File::create(file).unwrap();
        let mut buffer: Vec<u8> = Vec::new();

        buffer.push(self.header.id_len);
        buffer.push(self.header.color_map_type);
        buffer.push(self.header.data_type_code);
        buffer.extend(get_u8(self.header.color_map_origin, Order::LittleEndian));
        buffer.extend(get_u8(self.header.color_map_length, Order::LittleEndian));
        buffer.push(self.header.color_map_depth);
        buffer.extend(get_u8(self.header.x_origin, Order::LittleEndian));
        buffer.extend(get_u8(self.header.y_origin, Order::LittleEndian));
        buffer.extend(get_u8(self.header.width, Order::LittleEndian));
        buffer.extend(get_u8(self.header.height, Order::LittleEndian));
        buffer.push(self.header.bits_per_pixel);
        buffer.push(self.header.image_descriptor);

        for item in &self.image {
            buffer.extend(item.get_u8(PixelFormat::BGR));
        }

        file.write_all(buffer.as_slice()).unwrap();
    }
}