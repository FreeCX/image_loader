#[derive(Debug, Copy, Clone)]
pub struct Color {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
}

pub enum PixelFormat {
    RGB,
    BGR, 
    // RGBA,
}

pub enum Order {
    BigEndian,
    LittleEndian,
}

pub fn get_u16(buf: &[u8], order: Order) -> u16 {
    let hi = buf[0] as u16;
    let lo = buf[1] as u16;
    match order {
        Order::BigEndian => (hi << 8) + lo,
        Order::LittleEndian => (lo << 8) + hi,
    }
}

pub fn get_u8(buf: u16, order: Order) -> Vec<u8> {
    let hi = ((buf >> 8) & 0xff) as u8;
    let lo = (buf & 0xff) as u8;
    match order {
        Order::BigEndian => vec![hi, lo],
        Order::LittleEndian => vec![lo, hi],
    }
}

impl Color {
    pub fn black() -> Color {
        Color {
            red: 0,
            green: 0,
            blue: 0,
        }
    }

    pub fn rgb_u(r: u8, g: u8, b: u8) -> Color {
        Color {
            red: r,
            green: g,
            blue: b,
        }
    }

    pub fn rgb_v(data: &[u8]) -> Color {
        Color {
            red: data[0],
            green: data[1],
            blue: data[2],
        }
    }

    pub fn bgr_u(b: u8, g: u8, r: u8) -> Color {
        Color::rgb_u(r, g, b)
    }

    pub fn bgr_v(data: &[u8]) -> Color {
        Color {
            red: data[2],
            green: data[1],
            blue: data[0],
        }
    }

    pub fn get_u8(self, order: PixelFormat) -> Vec<u8> {
        use self::PixelFormat::*;
        match order {
            RGB => vec![self.red, self.green, self.blue],
            BGR => vec![self.blue, self.green, self.red],
        }
    }
}