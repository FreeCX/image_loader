#![allow(dead_code)]

#[macro_use]
pub mod macros;
mod extra;
mod tga;
pub use extra::Color;
pub use tga::TGAImage;