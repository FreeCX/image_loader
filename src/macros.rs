#[macro_export]
macro_rules! try_it {
    ($expr:expr) => {
        match ($expr).ok() {
            Some(value) => value,
            None => return None
        }
    }
}

#[macro_export]
macro_rules! color {
    ($r:expr, $g:expr, $b:expr) => (il::Color::rgb_u($r, $g, $b))
}